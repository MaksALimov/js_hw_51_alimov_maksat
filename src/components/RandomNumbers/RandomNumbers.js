import React from 'react';
import './RandomNumbers.css'

const RandomNumbers = props => {
    return (
        <div className="numbers">
            <span>{props.numbers}</span>
        </div>
    );
};

export default RandomNumbers;