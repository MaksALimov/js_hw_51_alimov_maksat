import './App.css';
import {Component} from "react";
import RandomNumbers from "./components/RandomNumbers/RandomNumbers";

class App extends Component {

    state = {
        numbers: [5, 11, 16, 23, 32]
    };

    getRandomNumbers = () => {
        const newArray = [];

        while (true) {
            const randomNumber = Math.floor(Math.random() * (37 - 5) + 5);
            if (!newArray.includes(randomNumber)) {
                newArray.push(randomNumber)
                if (newArray.length === 5) {
                    break;
                }
            }
        }

        newArray.sort((a, b) => {
            return a > b ? 1 : -1;
        });

        this.setState({
            numbers: newArray
        });
    };

    render() {
        return (
            <>
                <div className="newNumberContainer">
                    <button onClick={this.getRandomNumbers}>
                        <span/>
                        <span/>
                        <span/>
                        <span/>
                        New Numbers
                    </button>
                </div>
                {this.state.numbers.map((numbers, i) => (
                    <RandomNumbers
                        key={i}
                        numbers={numbers}
                    />
                ))}
            </>
        );
    };
}

export default App;
